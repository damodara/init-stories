import { Command, flags } from '@oclif/command';
import * as shell from 'shelljs';
import { writeFileSync, fstat } from 'fs';

class InitStories extends Command {
    static description = 'init svelte stories'

    static flags = {
        // add --version flag to show CLI version
        version: flags.version({ char: 'v' }),
        help: flags.help({ char: 'h' }),
        // flag with a value (-n, --name=VALUE)
        namespace: flags.string({ char: 'n', description: 'namespace for stories' }),
        mockdatas: flags.string({ char: 'm', description: 'mockdata modules' }),
        // flag with no value (-f, --force)
        force: flags.boolean({ char: 'f' }),
    }

    static args = [{ name: 'file' }]

    async run() {
        const { args, flags } = this.parse(InitStories);
        const pathItems = process.cwd().split('/');
        const srcPath = pathItems.slice(pathItems.indexOf('src')).map((v) => '..').join('/');

        const svelteFileNames = shell.ls(['*.svelte']);
        const storyFileNames = svelteFileNames.map((f) => f.replace('.svelte', '.stories.js'));

        shell.mkdir(['Stories']);
        shell.cd('Stories');

        const getMockDataImport = (md: string) => (
            `import {  } from '${srcPath}/mock-data/${md}';`
        );

        const getContent = (fn: string) => {
            const compName = fn.split('.')[0];
            const mockDataImports = (flags.mockdatas || '').split(' ').map(getMockDataImport).join('\n');
            const content = `
import '${srcPath}/storybook-init';
${mockDataImports}
import ${compName} from '../${compName}.svelte';


export default {
    title: '${flags.namespace ? `${flags.namespace}/` : ''}${compName}',
};


export const ideal = () => ({
    Component: ${compName},
    props: {

    },
});

`;
            return content;
        };

        const existingStories = shell.ls(['*.stories.js']);

        storyFileNames.forEach((f) => {
            if (existingStories.includes(f) && !flags.force) return;
            writeFileSync(f, getContent(f));
        });
    }
}

export = InitStories
