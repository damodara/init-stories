init-stories
============

inits svelte stories

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/init-stories.svg)](https://npmjs.org/package/init-stories)
[![Downloads/week](https://img.shields.io/npm/dw/init-stories.svg)](https://npmjs.org/package/init-stories)
[![License](https://img.shields.io/npm/l/init-stories.svg)](https://github.com/damooo/init-stories/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g init-stories
$ init-stories COMMAND
running command...
$ init-stories (-v|--version|version)
init-stories/0.0.0 linux-x64 node-v8.10.0
$ init-stories --help [COMMAND]
USAGE
  $ init-stories COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->

<!-- commandsstop -->
